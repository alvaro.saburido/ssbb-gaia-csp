# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).
## 0.2.0 - 2019-01-07
### Added
- CSP Project v2.0 (Poses, Renders and Cosmetics, Screnes, Models and textures, Resources)
- Shadow Mewtwo
- Mega Charizard X/Y
- Kingdom Hearts Cloud
- Zora Armor & Magic Armor Link
- Crash Bandicoot
- Morrigan & Lilith Darkstalkers
- New CSP character script
