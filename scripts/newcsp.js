var readline = require('readline');
var jsondir = require('jsondir');
var fs = require('fs-extra')

const COSMETICS_PATH = './Renders and Cosmetics/'
const askPath = () => {
    return new Promise((resolve, reject) => {
      rl.question('Where do you want to create it? ', (answer) => {
        console.log(`Output Directory: ${COSMETICS_PATH}${answer}`)
        resolve(answer)
      })
    })
  }
  
  const askName = () => {
    return new Promise((resolve, reject) => {
      rl.question('New CSP Character Name? ', (answer) => {
        console.log(`Character Name: ${answer}`)
        resolve(answer)
      })
    })
  }
  const createDir = (where, name) => {
      return new Promise((resolve, reject) => {
        console.log(`Creating...`)
        const json = {
            "-path": COSMETICS_PATH + where,
            [name]: {
                "CSPs": {
                    "HD": {
                        "-type": 'd'
                    },
                },
                "BPs": {
                    "HD": {
                        "-type": 'd'
                    },
                },
                "STCs": {
                    "HD": {
                        "-type": 'd'
                    },
                },
                "Stock Heads": {
                    "-type": 'd'
                },
                "Crops And Masks": {
                    "-type": 'd'
                },
            }
        }
        jsondir.json2dir(json, (err) => {
            if (err) reject(err);
            
            resolve({
                where,
                name,
            });
        });
      })
  }

  async function copyFiles (pathInfo) {
    try {
      await fs.copy(`${COSMETICS_PATH}/${pathInfo.where}/Crops And Masks/`, `${COSMETICS_PATH}/${pathInfo.where}//${pathInfo.name}/Crops And Masks/`);
      console.log('success!')
    } catch (err) {
      console.error(err)
    }
  }


const main = async () => {
    
    const outputDir = await askPath();
    const name = await askName();
    try {
        const res = await createDir(outputDir, name);
        await copyFiles(res)
    } catch (error) {
        console.error(error);
    }
    rl.close();
    process.exit(0);
  }

  var rl = readline.createInterface(process.stdin, process.stdout);
  rl.setPrompt('SSBB Gaia New Csp> ');
  rl.prompt();
  main();