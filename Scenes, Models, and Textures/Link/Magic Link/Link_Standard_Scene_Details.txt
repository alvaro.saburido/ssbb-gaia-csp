Model scale:
X: 0.200
Y: 0.200
Z: 0.200

Left eye iris/highlight location:
X: 0.150
Y: -0.020
Z: 0.000

Right eye iris/highlight location:
X: 0.080
Y: -0.020
Z: 0.000

Various UV edits including polygon2 (body), polygon26 (shield), polygon23 (sword blade)
