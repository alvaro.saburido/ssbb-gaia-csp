
To be able to use my mod, just rename the file you want to use to the name of the brawl costume you want to replace

Exemple (to replace default Project M Mewtwo):
FitMewtwo00.pac
FitMewtwo00.pcs

You need to put both the .pac and the .pcs on your SD card.


The "_C00" in the name of my files represent the Wii U costume number.  You can use my image gallery to identify which costume is which

C00 : default purple mewtwo costume
C01 : Orange
C02 : Blue
C03 : Brown
C04 : Red
C05 : Yellow
C06 : Green
C07 : Dark purple

----------------------------------------------------

These one are not wii U recolors but PM recolors/variant or original version I made

C08 : Project M Black and White
C09 : Dark (Evil) Mewtwo (original I made.  a black texture with heavy rim light and blue glowing eyes)
C10 : Dark Purple and Red eyes Shadow Mewtwo inspired from Theytah's Shadow Mewtwo skin for project M Mewtwo (new from update 1)


Armored C00 : Project M armored (grey armor, default purple mewtwo)
Armored C01 : Gold armor, black and White mewtwo
Armored C02 : Black and red armor, Dark purple Mew two.

Armored NoHelmet C00 : Project M armored without helmet (grey armor, default purple mewtwo)
Armored NoHelmet C01 : Gold armor without helmet, black and White mewtwo
Armored NoHelmet C02 : Black and red armor without helmet, Dark purple Mew two.

---------------------------------------------------


Have fun!

jaystring


--- Update 1 --------------------------------------

- Included a new costume (C10). A recolor inspired from a mod from Theytah
- Added CSPs and icons.

	- The regulars are the normal sized one to be imported inside project M's pac files.
	- The HD version are for Dolphin users to be used with the HD retexture project.  It's important that they be rename appropriately to work in the HD retexture project.

You can find the name that you have to give each texture by using dolphin to dump all the original texture in a folder and find the original texture's name that you want to replace.




--- Update 2 --------------------------------------

- fix all blue eyes glow when grabbing for all costumes
- optimized all models for better filesize
- updated all CSPs (that was to have them fit with the Pokken shadow Mewtwo's mod CSPs that has a seperate release on Brawlvault)
- Added HD textures to all models (included the pokken shadow Mewtwo textures that you can find on Brawlvault)
- fix the model based on Theytah's Shadow Mewtwo skin for project M Mewtwo.  ajusted the eyes to look similar to his export
